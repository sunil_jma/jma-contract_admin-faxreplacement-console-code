﻿using Google.Apis.Gmail.v1.Data;
using ImageMagick;
using Jma.Faxreplacement.ConsoleApp.Helper;
using Jma.Faxreplacement.ConsoleApp.Models;
using Jma.Faxreplacement.ConsoleApp.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting.Server;
using Microsoft.AspNetCore.Hosting.Server.Features;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Owin;
using Microsoft.AspNetCore.Hosting;

using Microsoft.AspNetCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Reflection;

namespace Jma.Faxreplacement.ConsoleApp
{
    class Program
    {
        private static DateTime ApplicationStarted = DateTime.Now;
        private static bool IsClosing = false;
        private static List<Task> MasterTaskList = new List<Task>();

        private static LoggerService logSvc = LoggerService.Instance;
        private static SharedDataService sds = SharedDataService.Instance;

        private static GmailApiService gmailSvc;

        private static Timer timer = null;

        /// <summary>
        /// Main application entry point
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            // MagickNET.Initialize(@"\magicnetxml");
            var runweb = args.Contains("runweb") || args.Contains("--runweb");

            // determine if the app is running as a service or console
            var isService = !(Debugger.IsAttached || args.Contains("--console") || args.Contains("console"));
            var builder = runweb ? CreateWebHostBuilder(
                        args.Where(arg => arg != "--console").ToArray()): null;

            if (isService && runweb)
            {
                var pathToExe = Process.GetCurrentProcess().MainModule.FileName;
                var pathToContentRoot = Path.GetDirectoryName(pathToExe);
                builder.UseContentRoot(pathToContentRoot);
            }

            if (runweb)
            {
                var host = builder.Build();

                if (isService)
                {
                    // To run the app without the CustomWebHostService change the
                    // next line to host.RunAsService();
                    host.RunAsCustomService();
                }
                else
                {
                    host.Run();
                }
            }

            System.Console.BackgroundColor = ConsoleColor.Black;
            AppDomain.CurrentDomain.ProcessExit += new EventHandler(ConsoleClosingEventHandler);
            ProcessCommand();
            // if there is no trigger defined, then exit the app
            if (ConfigHelper.APP_TRIGGER == TriggerType.None) return;

            // there is a trigger defined, so keep the app running until quit command is issued
            var rl = "";
            do
            {
                rl = System.Console.ReadLine().ToLower();
                switch (rl)
                {
                    case "about":
                        ShowAbout();
                        break;
                    case "help":
                        ShowHelp();
                        break;
                    case "status":
                        ShowStatus();
                        break;
                }
            } while (rl != "quit");
        }

        //private static void StartWebServer()
        //{
        //    var host = new WebHostBuilder()
        //        //.UseNowin()
        //        .UseContentRoot(Directory.GetCurrentDirectory())
        //        //.UseIISIntegration()
        //        .UseStartup<Startup>()
        //        .Build();

        //    host.Run();
        //}

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
        WebHost.CreateDefaultBuilder(args)
            .ConfigureLogging((hostingContext, logging) =>
            {
                logging.AddEventLog();
            })
            .ConfigureAppConfiguration((context, config) =>
            {
                // Configure the app here.
                var bp = Directory.GetCurrentDirectory();
                config.SetBasePath(bp);
                //config.AddInMemoryCollection(arrayDict);
                //config.AddJsonFile("json_array.json", optional: false, reloadOnChange: false);
                config.AddJsonFile("appsettings.json", optional: false, reloadOnChange: false);
                //config.AddXmlFile("abc.xml", optional: false, reloadOnChange: false);
                //config.AddEFConfiguration(options => options.UseInMemoryDatabase("InMemoryDb"));
                config.AddCommandLine(args);
            })
            .UseKestrel()
            .UseContentRoot(Directory.GetCurrentDirectory())
            .UseIISIntegration()
            //.UseStartup<Application.AppComponents.Startup>()
            .UseSetting(WebHostDefaults.ApplicationKey, typeof(Program).GetTypeInfo().Assembly.FullName) // Ignore the startup class assembly as the "entry point" and instead point it to this app
                    .UseStartup<Startup>();

        #region System.Console. message interactions

        /// <summary>
        /// Keeps count of the messages processed by a running instance of the application
        /// </summary>
        private static void AddMessageCount()
        {
            System.Threading.Interlocked.Increment(ref sds.MessagesProcessed);
        }

        /// <summary>
        /// Keeps count of the subscription notifications processed by the application during a running instance
        /// </summary>
        private static void AddSubscriberNotificationsCount()
        {
            System.Threading.Interlocked.Increment(ref sds.SubscriberNotifications);
        }

        /// <summary>
        /// Shows application stats in the System.Console.
        /// </summary>
        private static void ShowStatus()
        {
            TimeSpan ts = DateTime.Now - ApplicationStarted;
            var msg =
$@"#######################################################################################
App Status:
- Pending Tasks: {MasterTaskList.Count}
- Application Started: {ApplicationStarted.ToLongDateString()}
- Running time: {Math.Round(ts.TotalMinutes, 2)} minutes
- PubSub Notifications: {sds.SubscriberNotifications}
- Messages Processed: {sds.MessagesProcessed}
#######################################################################################";
            System.Console.ForegroundColor = ConsoleColor.Yellow;
            System.Console.WriteLine(msg);
            SetDefaultConsoleColors();
        }

        /// <summary>
        /// Shows the help message in the console
        /// </summary>
        private static void ShowHelp()
        {
            System.Console.ForegroundColor = ConsoleColor.Magenta;
            System.Console.WriteLine(ConfigHelper.APP_HELP);
            SetDefaultConsoleColors();
        }

        /// <summary>
        /// Sets the default console font color
        /// </summary>
        private static void SetDefaultConsoleColors()
        {
            System.Console.ForegroundColor = ConsoleColor.Green;
        }

        /// <summary>
        /// Shows a critical message with a specific font color and then resets to default color
        /// </summary>
        /// <param name="msg"></param>
        private static void ShowConsoleCriticalMessage(string msg)
        {
            System.Console.ForegroundColor = ConsoleColor.Red;
            System.Console.WriteLine(msg);
            SetDefaultConsoleColors();
        }

        /// <summary>
        /// Shows an alert messsage with a specific font color and then resets to default color
        /// </summary>
        /// <param name="msg"></param>
        private static void ShowConsoleAlertMessage(string msg)
        {
            System.Console.ForegroundColor = ConsoleColor.Yellow;
            System.Console.WriteLine(msg);
            SetDefaultConsoleColors();
        }

        /// <summary>
        /// Show the about message in the console
        /// </summary>
        private static void ShowAbout()
        {
            System.Console.ForegroundColor = ConsoleColor.Cyan;
            System.Console.WriteLine(ConfigHelper.APP_ABOUT);
            SetDefaultConsoleColors();
        }

        #endregion Console message interactions

        /// <summary>
        /// Handle the console close / quit cleanly
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event arguments</param>
        private static void ConsoleClosingEventHandler(object sender, EventArgs e)
        {
            IsClosing = true;
            ShowConsoleCriticalMessage("Application Exiting. Cleaning up... please wait...");
            Task.WaitAll(MasterTaskList.ToArray());
            if (gmailSvc != null) gmailSvc.Dispose();
            ShowConsoleAlertMessage("Done. Goodbye.");
        }

        /// <summary>
        /// Main application execution process
        /// </summary>
        private static void ProcessCommand()
        {
            ApplicationInit();

            // this is an initial scan to make sure that backlogs of unread
            // messages are cleared before turning on the event/trigger based watch
            CoreRoutine();
            switch (ConfigHelper.APP_TRIGGER)
            {
                case TriggerType.Timer:
                    timer = new Timer(TimerCallback, null, ConfigHelper.APP_TIMEINTERVAL_SECS * 1000, ConfigHelper.APP_TIMEINTERVAL_SECS * 1000);
                    break;
                case TriggerType.PubSub:
                    // start the pubsub subscription watch for real time notification of message arrival
                    InitializeGooglePubSubscription();
                    break;
                case TriggerType.None:
                // use an external scheduling mechanism like windows task scheduler
                // so we have to exit the application
                default:
                    break;
            }
        }

        private static void TimerCallback(object o)
        {
            logSvc.LogInfo($"Timer triggered every {ConfigHelper.APP_TIMEINTERVAL_SECS} seconds");
            CoreRoutine();
        }

        private static void CoreRoutine()
        {
            if (IsClosing) return;
            // Get unread messages from the user's inbox
            GetGmailUnreadMessages();
            Task.WaitAll(MasterTaskList.ToArray());
            MasterTaskList.Clear();
        }

        /// <summary>
        /// initialize the application environment
        /// </summary>
        private static void ApplicationInit()
        {
            // House keeping
            System.Console.Clear();
            ShowAbout();
            ShowHelp();
            if (!System.IO.Directory.Exists(ConfigHelper.CREDS_DIR))
            {
                System.IO.Directory.CreateDirectory(ConfigHelper.CREDS_DIR);
            }
            if (!System.IO.Directory.Exists(ConfigHelper.TEMPFOLDER))
            {
                System.IO.Directory.CreateDirectory(ConfigHelper.TEMPFOLDER);
            }
            if (!System.IO.Directory.Exists(ConfigHelper.OUTFOLDER))
            {
                System.IO.Directory.CreateDirectory(ConfigHelper.OUTFOLDER);
            }

            // instantiate the GmailApiService class
            gmailSvc = new Services.GmailApiService(ConfigHelper.APPID, ConfigHelper.CLIENTSECRETFILE, ConfigHelper.CLIENTTOKENDIR);

            // Create Gmail user credentials.
            gmailSvc.AuthorizeGmailClient(ConfigHelper.SCOPES);

            // Create Gmail API service.
            gmailSvc.CreateGmailApiService();

        }

        /// <summary>
        /// Get all new unread messages from the gmail account
        /// </summary>
        internal static void GetGmailUnreadMessages()
        {
            logSvc.LogInfo("Begin Gmail query");
            ListMessagesResponse resMsgs = gmailSvc.GetUnreadMessages();
            if (resMsgs == null || resMsgs.Messages == null || resMsgs.Messages.Count < 0)
            {
                logSvc.LogInfo($"No unread messages.");
            }
            else
            {
                logSvc.LogInfo($"Found {resMsgs.Messages.Count} unread messages.");

                foreach (var m in resMsgs.Messages)
                {
                    if (IsClosing) return;
                    AddMessageCount();
                    var tpm = ProcessMessageAsync(m.Id);
                    MasterTaskList.Add(tpm);
                    var awtpm = tpm.GetAwaiter();
                    awtpm.OnCompleted(
                        () =>
                        {
                            var ares = awtpm.GetResult();
                            logSvc.LogInfo($"Message [{ares}]: Async processing completed.");
                            var q = sds.MessageQ.items.Find(f => f.messageId == ares);
                            if (q != null)
                            {
                                q.pages = PdfService.GetNumberOfPages(ConfigHelper.OUTFOLDER, q.outFileName + ".pdf");
                                FileIOHelper.CreateXmlMetaDataFile(q, ConfigHelper.OUTFOLDER, q.pages);
                                logSvc.LogInfo($"Message [{ares}]: XML created.");
                            }
                            MasterTaskList.Remove(tpm);
                        });
                }
            }
            logSvc.LogInfo("End Gmail query.");
        }

        /// <summary>
        /// Gets (Async) the message object using the Gmail API with the message id
        /// </summary>
        /// <param name="messageid">a unique identifier for each message</param>
        /// <returns></returns>
        private static async Task<string> ProcessMessageAsync(string messageid)
        {
            var ts = new List<Task>();
            var t1 = Task.Delay(5000);
            ts.Add(t1);
            var t = Task.Run(() => ProcessMessage(messageid));
            ts.Add(t);
            var res = await t;
            Task.WaitAll(ts.ToArray());
            return res;
        }

        /// <summary>
        /// Gets the message object using the Gmail API with the message id
        /// </summary>
        /// <param name="messageid">a unique identifier for each message</param>
        /// <returns></returns>
        private static string ProcessMessage(string messageid)
        {
            logSvc.LogInfo($"Message [{messageid}]: Begin processing.");

            List<Task<PdfSvcResponse>> tlist = new List<Task<PdfSvcResponse>>();
            var message_raw = "";
            var msgtxt = "";
            var msghtml = "";
            var response = gmailSvc.GetMessage(messageid);
            //foreach (var h in response.Payload.Headers)
            //{
            //}
            Item itm = null;
            sds.MessageQ.items.Add(itm =
                new Item()
                {
                    messageId = messageid,
                    receivedDate = response.Payload.Headers.FirstOrDefault(h => h.Name == "Date").Value,
                    subject = response.Payload.Headers.FirstOrDefault(h => h.Name == "Subject").Value,
                    fromId = response.Payload.Headers.FirstOrDefault(h => h.Name == "From").Value,
                    inFileItems = new List<FItem>()
                });
            var date_part = CommonHelper.FormatDate(itm.receivedDate);
            var sub_part = itm.subject;
            itm.outFileName = date_part + "_" + sub_part + '_' + messageid;
            for (var i = 0; i < response.Payload.Parts.Count; ++i)
            {
                var part = response.Payload.Parts[i];
                var isAttch = !string.IsNullOrEmpty(part.Body.AttachmentId);
                if (!isAttch)
                {
                    if (part.Body.Data != null)
                    {
                        var buff = CommonHelper.FromBase64ForUrlString(part.Body.Data);
                        if (part.MimeType == "text/html")
                        {
                            msghtml += Encoding.UTF8.GetString(buff) + "<hr>";
                        }
                        else
                        {
                            msgtxt += Encoding.UTF8.GetString(buff) + "<hr>";
                        }
                    }
                    else
                    {
                        if (part.Parts != null && part.Parts.Count > 0)
                        {
                            for (var j = 0; j < part.Parts.Count; ++j)
                            {
                                var p2 = part.Parts[j];
                                if (p2.Body.Data != null)
                                {
                                    var buff = CommonHelper.FromBase64ForUrlString(p2.Body.Data);
                                    if (p2.MimeType == "text/html")
                                    {
                                        msghtml += Encoding.UTF8.GetString(buff) + "<hr>";
                                    }
                                    else
                                    {
                                        //var buff = Convert.FromBase64String(p2.Body.Data);
                                        msgtxt += Encoding.UTF8.GetString(buff) + "<hr>";
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    // handle attachment
                    var mpb = gmailSvc.GetAttachment(messageid, part.Body.AttachmentId);
                    AddToList(messageid, messageid + "_" + part.Filename, 0);
                    // download as pdf
                    var d = CommonHelper.FromBase64ForUrlString(mpb.Data);
                    FileIOHelper.WriteByteArrayToFile(d, ConfigHelper.TEMPFOLDER + messageid + "_" + part.Filename);
                    logSvc.LogInfo($"Message [{messageid}]: Attachment ({part.Filename}) downloaded.");
                    // base64topdf.base64Decode(response.data.data, tmpf + this.aMsgId + '_' + this.aFilename);
                }
            }

            if (msghtml.Length > msgtxt.Length)
            {
                message_raw = msghtml;
                // FileIOHelper.WriteTextToFile(message_raw, ConfigHelper.TEMPFOLDER + messageid + ".html");
            }
            else
            {
                message_raw = msgtxt;
                // FileIOHelper.WriteTextToFile(message_raw, ConfigHelper.TEMPFOLDER + messageid + ".txt");
            }

            if (!string.IsNullOrEmpty(message_raw))
            {
                AddToList(messageid, messageid + ".pdf", 1);
                var tps = PdfService.CreatePdfFromHtmlAsync(messageid, message_raw, ConfigHelper.TEMPFOLDER, messageid + ".pdf");
                var awt = tps.GetAwaiter();
                tlist.Add(tps);
                awt.OnCompleted(() =>
                {
                    var res = awt.GetResult();
                    if (res.IsException) logSvc.LogError($"Message [{messageid}] Error: " + res.Message, res.Ex);
                    else logSvc.LogInfo($"Message [{messageid}]: " + res.Message);
                    tlist.Remove(tps);
                }
                );
            }

            Task.WaitAll(tlist.ToArray());
            // check if combination of files is required
            var q = sds.MessageQ.items.Find(f => f.messageId == messageid);
            if (q.inFileItems != null && q.inFileItems.Count > 0)
            {
                if (q.inFileItems.Count > 1)
                {
                    var fn = q.inFileItems.OrderByDescending(i => i.displayOrder).Select(i => i.fileName).ToArray();
                    var pdfres = PdfService.CombineFilesToSingle(
                         messageid,
                         ConfigHelper.TEMPFOLDER,
                         ConfigHelper.OUTFOLDER,
                         fn,
                         CommonHelper.SanitizeFilename(q.outFileName)
                     );
                    if (pdfres.IsException) logSvc.LogError($"Message [{messageid}] Error: " + pdfres.Message, pdfres.Ex);
                    else
                    {
                        logSvc.LogInfo($"Message [{messageid}]: " + pdfres.Message);
                        logSvc.LogInfo($"Message [{messageid}]: File moved to out dir.");
                    }

                    try
                    {
                        FileIOHelper.DeleteFiles(fn);
                        logSvc.LogInfo($"Message [{messageid}]: Temp files deleted");
                    }
                    catch (Exception ex)
                    {
                        logSvc.LogError($"Message [{messageid}] Error: " + ex.Message, ex);
                    }
                }
                else
                {
                    FileIOHelper.MoveFile(q.inFileItems[0].fileName, ConfigHelper.OUTFOLDER, q.outFileName + ".pdf");
                    logSvc.LogInfo($"Message [{messageid}]: File moved to out dir.");
                    logSvc.LogInfo($"Message [{messageid}]: Temp files deleted");
                }
            }
            logSvc.LogInfo($"Message [{messageid}]: End processing.");
            return messageid;
        }

        /// <summary>
        /// Add entry to the message work queue
        /// Information of the files that make the message and attachment per message is held in the FItem
        /// Sequence of pdf file combination is also maintained for asynchronous operation
        /// </summary>
        /// <param name="messageid"></param>
        /// <param name="filename"></param>
        /// <param name="seq"></param>
        private static void AddToList(string messageid, string filename, int seq)
        {
            var q = sds.MessageQ.items.Find(f => f.messageId == messageid);
            if (q != null)
            {
                q.inFileItems.Add(new FItem()
                {
                    fileName = ConfigHelper.TEMPFOLDER + filename,
                    displayOrder = seq
                });
                return;
            }
            throw new Exception("Message ID was not found in Queue");
        }

        #region Google Pub Subscription code
        private static void InitializeGooglePubSubscription()
        {
            logSvc.LogInfo("Starting Gmail account subscription watch.");

            var res = gmailSvc.StartWatch(ConfigHelper.GOOGLE_PUBSUBTOPIC);
            if (res.IsException)
            {
                logSvc.LogError("GooglePubSubscription Error: " + res.Message, res.Ex);
                return;
            }
            else
                logSvc.LogInfo("GooglePubSubscription: " + res.Message);

            var fp = FileIOHelper.GetFullFilePath(ConfigHelper.GOOGLE_APPCREDSPATH);
            Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS",
                fp,
                EnvironmentVariableTarget.Process);
            RunSubscriptionInstance();
        }

        private static void RunSubscriptionInstance()
        {
            var gps = new PubSubService(new Google.Cloud.PubSub.V1.SubscriptionName(
                ConfigHelper.GOOGLE_CLOUDPROJECTID, ConfigHelper.GOOGLE_PUBSUBSCRIPTION
                ));

            var tsub = gps.PullSubscription();
            MasterTaskList.Add(tsub);
            var atsub = tsub.GetAwaiter();
            atsub.OnCompleted(() =>
            {
                MasterTaskList.Remove(tsub);
                CoreRoutine();
                if (!IsClosing)
                {
                    RunSubscriptionInstance();
                }
            });
        }
        #endregion Google Pub Subscription code
    }
}