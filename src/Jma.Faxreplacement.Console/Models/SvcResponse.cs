﻿using System;

namespace Jma.Faxreplacement.ConsoleApp.Models
{
    public class SvcResponse
    {
        public string UID { get; private set; }
        public bool IsException { get; private set; }
        public string Message { get; private set; }
        public Exception Ex { get; private set; }

        public SvcResponse(string uid, bool isError, string errMsg, Exception e)
        {
            UID = uid;
            IsException = isError;
            Message = errMsg;
            Ex = e;
        }
    }
}
