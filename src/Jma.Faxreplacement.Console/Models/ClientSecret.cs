﻿namespace Jma.Faxreplacement.ConsoleApp.Models
{
    public class ClientSecret
    {
        public Installed installed { get; set; }
    }

    public class Installed
    {
        public string client_id;
        public string project_id;
        public string auth_uri;
        public string token_uri;
        public string auth_provider_x509_cert_url;
        public string client_secret;
        public string[] redirect_uris;
    }
}
