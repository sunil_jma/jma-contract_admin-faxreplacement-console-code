﻿using System.Collections.Generic;

namespace Jma.Faxreplacement.ConsoleApp.Models
{
    public class MessageQ
    {
        public List<Item> items;
        public MessageQ()
        {
            items = new List<Item>();
        }
    }

    public class Item
    {
        public string messageId;
        public string fromId;
        public string subject;
        public string receivedDate;
        public List<FItem> inFileItems;
        public string outFileName;
        public int pages;
    }

    public class FItem
    {
        public string fileName;
        public int displayOrder;
    }
}
