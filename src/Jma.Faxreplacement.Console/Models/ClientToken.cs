﻿namespace Jma.Faxreplacement.ConsoleApp.Models
{
    class ClientToken
    {
        public string access_token;
        public string refresh_token;
        public string scope;
        public string token_type;
        public long expiry_date;
    }
}
