﻿using System;
using System.Configuration;
namespace Jma.Faxreplacement.ConsoleApp.Helper
{
    public static class ConfigHelper
    {
        internal static string APPID = ConfigurationManager.AppSettings["applicationid"];
        internal static TriggerType APP_TRIGGER = (TriggerType)Enum.Parse(typeof(TriggerType), ConfigurationManager.AppSettings["applicationtrigger"]);
        internal static int APP_TIMEINTERVAL_SECS = int.Parse(ConfigurationManager.AppSettings["applicationtimesecs"]);

        internal static string APP_PATH =
            System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

        internal static string CREDS_DIR = APP_PATH + "\\" +
            ConfigurationManager.AppSettings["credentialsdir"] + "\\";

        internal static string CLIENTSECRETFILE = APP_PATH + "\\" + ConfigurationManager.AppSettings["clientsecretfile"];

        internal static string CLIENTTOKENDIR = CREDS_DIR + ConfigurationManager.AppSettings["clienttokendir"];

        internal static string TEMPFOLDER = APP_PATH + "\\" +
            ConfigurationManager.AppSettings["tempfolder"] + "\\";

        internal static string OUTFOLDER = APP_PATH + "\\" +
            ConfigurationManager.AppSettings["outfolder"] + "\\";

        internal static string[] SCOPES = {
            "https://www.googleapis.com/auth/gmail.readonly",
            "https://www.googleapis.com/auth/gmail.modify",
            "https://mail.google.com/",
            "https://www.googleapis.com/auth/cloud-platform",
            "https://www.googleapis.com/auth/pubsub"
        };

        internal static string GOOGLE_MAILACOUNT = ConfigurationManager.AppSettings["gmailaccount"];

        internal static string GOOGLE_CLOUDPROJECTID = ConfigurationManager.AppSettings["gcloudprojectid"];

        internal static string GOOGLE_PUBSUBTOPIC = ConfigurationManager.AppSettings["gpubsubtopic"];

        internal static string GOOGLE_PUBSUBSCRIPTION = ConfigurationManager.AppSettings["gpubsubscription"];

        internal static string GOOGLE_APPCREDSPATH = ConfigurationManager.AppSettings["googleappcredsfile"];

        internal static string APP_HELP =
@"#######################################################################################
Console Commands:
--- Type ABOUT for the about information
--- Type HELP for the help list
--- Type STATUS for the application status
--- Type QUIT to terminate the application
* Note: commands are not case sensitive
#######################################################################################";

        internal static string APP_ABOUT =
@"#######################################################################################
#######################################################################################
########                                                                       ########
########   #######     #     ##     ##        #######   ##      ##   ######    ########
########   #######    ###     ##   ##         #######   ####  ####   #######   ########
########   ##       ##   ##    ## ##          ##        ## #### ##   ##    ##  ########
########   #####    ##   ##     ###     ###   ##        ##  ##  ##   ##    ##  ########
########   ####     #######     ###     ###   ##        ##  ##  ##   ##    ##  ########
########   ##       #######    ## ##          ##        ##      ##   ##    ##  ########
########   ##       ##   ##   ##   ##         #######   ##      ##   #######   ########
########   ##       ##   ##  ##     ##        #######   ##      ##   ######    ########
########                                                                       ########
#######################################################################################
#######################################################################################";
    }

    public enum TriggerType
    {
        None = 0,
        Timer = 1,
        PubSub = 2
    }
}
