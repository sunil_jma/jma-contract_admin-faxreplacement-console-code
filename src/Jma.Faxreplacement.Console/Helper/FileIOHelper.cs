﻿using Jma.Faxreplacement.ConsoleApp.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Jma.Faxreplacement.ConsoleApp.Helper
{
    public static class FileIOHelper
    {
        public static void WriteByteArrayToFile(byte[] buffer, string fullFilePath)
        {
            File.WriteAllBytesAsync(fullFilePath, buffer);
        }
        public static void WriteTextToFile(string message, string fullFilePath)
        {
            File.WriteAllTextAsync(fullFilePath, message);
        }

        public static string ReadFileString(string fileName)
        {
            var stringres = "";
            var fi = new FileInfo(fileName);
            if (fi.Exists)
            {
                using (var fs = fi.OpenRead())
                {
                    using (var sr = new StreamReader(fs))
                    {
                        stringres = sr.ReadToEnd();
                    }
                }
                if (string.IsNullOrEmpty(stringres))
                {
                    // log the error
                }
            }
            return stringres;
        }

        internal static void CreateXmlMetaDataFile(Item q, string outDir, int pages, int unprocessedfiles = 0)
        {
            string uniqueId = q.messageId;
            string emailId = q.fromId;
            string messageDateTime = q.receivedDate;
            string badPageCount = unprocessedfiles.ToString();
            string goodPageCount = q.pages.ToString();
            string pdfFileName = q.outFileName;

            string processed = "0";
            string jobStatus = "0";
            string remoteId = "";
            var xml=$@"<?xml version=""1.0"" encoding=""ISO-8859-1""?>
<ImportSession>
<Batches>
<Batch BatchClassName=""{uniqueId}"" Processed=""{processed}"">
<Documents>
<Document DocumentNumber=""{uniqueId}"">
<IndexFields>
<IndexField Name=""UserId"" Value=""{emailId}""/>
<IndexField Name=""UniquelD"" Value=""{uniqueId}""/>
<IndexField Name=""JobCreateTime"" Value=""{messageDateTime}""/>
<IndexField Name=""FaxServer.value=""Gmail""/>
<IndexField Name=""BadPageCout"" Value=""{badPageCount}""/>
<IndexField Name=""GoodPageCount"" Value=""{goodPageCount}""/>
<IndexField Name=""FaxStatus"" Value=""{jobStatus}""/>
<IndexField Name=""RemoteID"" Value=""{remoteId}""/>
</IndexFields>
<Pages>
<Page ImportFileName=""{pdfFileName}""/>
</Pages>
</Document>
</Documents>
</Batch>
</Batches>
</ImportSession>";
//            string xml = $@"<Message>
//    <From><![CDATA[{q.fromId}]]></From>                
//    <Subject><![CDATA[{q.subject}]]></Subject>
//    <Date>{q.receivedDate}</Date>
//    <PdfFile>{q.outFileName}</PdfFile>
//    <Pages>{pages}</Pages>
//</Message>";
            File.WriteAllTextAsync(outDir + q.messageId + ".xml", xml);
        }

        internal static void MoveFiles(string[] fileNames, string outFolder)
        {
            foreach (var fn in fileNames)
            {
                MoveFile(fn, outFolder);
            }
        }

        internal static string GetFullFilePath(string fn)
        {
            var f = new FileInfo(fn);
            return f.FullName;
        }

        internal static void MoveFile(string fn, string outFolder, string outFileName = "")
        {
            var f = new FileInfo(fn);
            if (!f.Exists) return;
            f.CopyTo(outFolder
                + (string.IsNullOrEmpty(outFileName) ? f.Name : outFileName),
                true);
            f.Delete();
        }

        internal static void DeleteFiles(string[] fileNames)
        {
            foreach (var fn in fileNames)
            {
                var f = new FileInfo(fn.Replace("\"", ""));
                if (!f.Exists) return;
                f.Delete();
            }
        }
    }
}
