﻿using System;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;

namespace Jma.Faxreplacement.ConsoleApp.Helper
{
    class CommonHelper
    {
        public static string FormatDate(string date_str)
        {
            CultureInfo myCul = new CultureInfo("en-US");
            var succ = false;
            var date = DateTime.MinValue;
            succ = DateTime.TryParse(date_str, myCul, DateTimeStyles.AssumeLocal, out date);
            if (!succ)
                succ = DateTime.TryParse(date_str, myCul, DateTimeStyles.AssumeUniversal, out date);
            if (!succ)
                succ = DateTime.TryParse(date_str, myCul, DateTimeStyles.AdjustToUniversal, out date);
            if (!succ)
                succ = DateTime.TryParse(date_str, myCul, DateTimeStyles.NoCurrentDateDefault, out date);
            if (!succ)
                succ = DateTime.TryParse(date_str, myCul, DateTimeStyles.None, out date);
            if (!succ)
                succ = DateTime.TryParse(date_str, out date);

            date_str = date_str.Replace(",", "");
            if (!succ)
                succ = DateTime.TryParseExact(date_str, "ddd dd MMM yyyy HH:mm:ss zz'00'", myCul, DateTimeStyles.None, out date);
            if (!succ)
                succ = DateTime.TryParseExact(date_str, "ddd MM dd yyyy HH:mm:ss zz'00'", myCul, DateTimeStyles.None, out date);
            if (!succ)
                succ = DateTime.TryParseExact(date_str, "ddd MMM dd yyyy HH:mm:ss +0000 (UTC)", myCul, DateTimeStyles.None, out date);
            if (!succ)
                succ = DateTime.TryParseExact(date_str, "ddd dd MMM yyyy HH:mm:ss +0000 (UTC)", myCul, DateTimeStyles.None, out date);
            if (!succ)
                succ = DateTime.TryParseExact(date_str, "ddd d MMM yyyy HH:mm:ss +0000 (UTC)", myCul, DateTimeStyles.None, out date);
            return date.ToString("MM_dd_yyyy");
        }

        public static byte[] FromBase64ForUrlString(string base64ForUrlInput)
        {
            int padChars = (base64ForUrlInput.Length % 4) == 0 ? 0 : (4 - (base64ForUrlInput.Length % 4));
            StringBuilder result = new StringBuilder(base64ForUrlInput, base64ForUrlInput.Length + padChars);
            result.Append(String.Empty.PadRight(padChars, '='));
            result.Replace('-', '+');
            result.Replace('_', '/');
            return Convert.FromBase64String(result.ToString());
        }

        public static string SanitizeFilename(string teststring)
        {
            var outStr = teststring;
            var illegalRe = @"/[\/\?<>\\:\*\| /"":]/g";
            //var controlRe = "/[\x00 -\x1f\x80 -\x9f] / g";
            var reservedRe = "/^\\.+$/";
            var windowsReservedRe = "/^ (con | prn | aux | nul | com[0 - 9] | lpt[0 - 9])(\\..*) ?$/ i";
            var windowsTrailingRe = "/[\\. ] +$/";
            outStr = Regex.Replace(outStr, illegalRe, "");
            //outStr = Regex.Replace(outStr, controlRe, "");
            outStr = Regex.Replace(outStr, reservedRe, "");
            outStr = Regex.Replace(outStr, windowsReservedRe, "");
            outStr = Regex.Replace(outStr, windowsTrailingRe, "");
            if (outStr.Length > 255) return outStr.Substring(0, 255);
            else return outStr;
        }
    }
}
