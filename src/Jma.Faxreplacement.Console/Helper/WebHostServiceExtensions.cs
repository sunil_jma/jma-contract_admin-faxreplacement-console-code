﻿using Jma.Faxreplacement.ConsoleApp.Services;
using Microsoft.AspNetCore.Hosting;
using System.ServiceProcess;

namespace Jma.Faxreplacement.ConsoleApp.Helper
{
    #region ExtensionsClass
    public static class WebHostServiceExtensions
    {
        public static void RunAsCustomService(this IWebHost host)
        {
            var webHostService = new CustomWebHostService(host);
            ServiceBase.Run(webHostService);
        }
    }
    #endregion
}
