﻿using Google.Cloud.PubSub.V1;
using Jma.Faxreplacement.ConsoleApp.Models;
using System.Collections.Generic;

namespace Jma.Faxreplacement.ConsoleApp.Services
{
    public sealed class SharedDataService
    {
        private static readonly SharedDataService instance = new SharedDataService();

        public List<PubsubMessage> ReceivedMessages = null;
        public int MessagesProcessed = 0;
        public int SubscriberNotifications = 0;
        public MessageQ MessageQ;

        static SharedDataService()
        {
            instance.ReceivedMessages = new List<PubsubMessage>();
            instance.MessageQ = new MessageQ();
        }

        public static SharedDataService Instance { get { return instance; } }
    }
}
