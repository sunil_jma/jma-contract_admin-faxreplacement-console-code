﻿using Google.Cloud.PubSub.V1;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Jma.Faxreplacement.ConsoleApp.Services
{
    public class PubSubService
    {
        private LoggerService logSvc = LoggerService.Instance;
        private SharedDataService sds = SharedDataService.Instance;

        private SubscriptionName subscriptionName = null;

        public PubSubService(SubscriptionName subscription)
        {
            subscriptionName = subscription;
        }

        internal async Task CreateSubscriptionAsync()
        {
            // Subscribe to the topic.
            SubscriberServiceApiClient subscriberService = await SubscriberServiceApiClient.CreateAsync();
            string projectId = null;
            string subscriptionId = null;
            TopicName topicName = null;
            SubscriptionName subscriptionName = new SubscriptionName(projectId, subscriptionId);
            subscriberService.CreateSubscription(subscriptionName, topicName, pushConfig: null, ackDeadlineSeconds: 60);
        }

        internal async Task PullSubscription()
        {
            // Pull messages from the subscription using SimpleSubscriber.
            SubscriberClient subscriber = await SubscriberClient.CreateAsync(subscriptionName);
            List<PubsubMessage> receivedMessages = new List<PubsubMessage>();
            // Start the subscriber listening for messages.
            try
            {
                await subscriber.StartAsync((msg, cancellationToken) =>
                {
                    sds.ReceivedMessages.Add(msg);
                    logSvc.LogInfo($"Received message {msg.MessageId} published at {msg.PublishTime.ToDateTime()}");
                    var payload = msg.Data.ToStringUtf8();
                        // Stop this subscriber after one message is received.
                        // This is non-blocking, and the returned Task may be awaited.
                        subscriber.StopAsync(TimeSpan.FromSeconds(15));
                        // Return Reply.Ack to indicate this message has been handled.
                        return Task.FromResult(SubscriberClient.Reply.Ack);
                });
            }
            catch (Exception ex)
            {
                logSvc.LogError(ex.Message, ex);
            }
        }
    }
}
