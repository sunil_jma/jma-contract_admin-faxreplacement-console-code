﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Gmail.v1;
using Google.Apis.Gmail.v1.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using Jma.Faxreplacement.ConsoleApp.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace Jma.Faxreplacement.ConsoleApp.Services
{
    public class GmailApiService
    {
        private string clientsecretfile;
        private string clienttokendir;
        public string ClientId { get; set; }
        public GmailService GmailSvc { get; set; }
        public UserCredential UserCreds { get; set; }
        public LoggerService logSvc = new LoggerService();

        internal void Dispose()
        {
            GmailSvc.Dispose();
            UserCreds = null;
        }

        public GmailApiService(string clientId, string cliSecretFile, string cliTokenDir)
        {
            ClientId = clientId;
            clientsecretfile = cliSecretFile;
            clienttokendir = cliTokenDir;
        }

        internal void AuthorizeGmailClient(string[] scopes)
        {
            using (var stream =
                new FileStream(clientsecretfile, FileMode.Open, FileAccess.Read))
            {
                var gcs = GoogleClientSecrets.Load(stream);
                // The .json token file stores the user's access and refresh tokens, and is created
                // automatically when the authorization flow completes for the first time.
                var fds = new FileDataStore(clienttokendir, true);
                UserCreds = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    gcs.Secrets,
                    scopes,
                    "user",
                    CancellationToken.None,
                    fds).Result;
                logSvc.LogInfo("Credential file saved to: " + clienttokendir);
            }
        }

        internal void CreateGmailApiService()
        {
            GmailSvc = new GmailService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = UserCreds,
                ApplicationName = ClientId,
            });
        }

        internal ListMessagesResponse GetUnreadMessages()
        {
            var messages = GmailSvc.Users.Messages.List("me");
            messages.MaxResults = 10;
            messages.LabelIds = new Google.Apis.Util.Repeatable<string>(new string[] { "UNREAD" });
            var resMsgs = messages.Execute();
            return resMsgs;
        }

        internal Message GetMessage(string messageId)
        {
            return GmailSvc.Users.Messages.Get("me", messageId).Execute();
        }

        internal MessagePartBody GetAttachment(string messageId, string attachmentId)
        {
            return GmailSvc.Users.Messages.Attachments.Get("me", messageId, attachmentId).Execute();
        }
        internal SvcResponse StartWatch(string topicName)
        {
            try
            {
                WatchRequest wb = new WatchRequest();
                wb.LabelIds = new string[] { "INBOX" };
                wb.TopicName = topicName;
                GmailSvc.Users.Watch(wb, "me").Execute();
                return new SvcResponse(topicName, false, $"Subscription topic [{topicName}] watch started.", null);
            }
            catch (Exception ex)
            {
                return new SvcResponse(topicName, true, ex.Message, ex);
            }
        }

        internal void GetGmailLabels()
        {
            // Define parameters of request.
            UsersResource.LabelsResource.ListRequest request = GmailSvc.Users.Labels.List("me");

            // List labels.
            IList<Label> labels = request.Execute().Labels;
            logSvc.LogInfo("Labels:");
            if (labels != null && labels.Count > 0)
            {
                foreach (var labelItem in labels)
                {
                    logSvc.LogInfo(labelItem.Name);
                }
            }
            else
            {
                logSvc.LogInfo("No labels found.");
            }
        }
    }
}
