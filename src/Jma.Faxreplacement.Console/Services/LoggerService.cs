﻿using JMFamily.Logging;
using System;

namespace Jma.Faxreplacement.ConsoleApp.Services
{
    public sealed class LoggerService
    {
        private static readonly LoggerService instance = new LoggerService();
        static LoggerService() { }

        public static LoggerService Instance { get { return instance; } }

        internal void LogInfo(string message)
        {
            var ds = DateTime.Now.ToUniversalTime().ToString();
            System.Console.WriteLine(ds + " (UTC): " + message);

            Logger.Info(message);
        }

        internal void LogError(string message, Exception e)
        {
            var ds = DateTime.Now.ToUniversalTime().ToString();
            System.Console.WriteLine(ds + " (UTC): " + message);

            Logger.Error(e);
        }

    }
}
