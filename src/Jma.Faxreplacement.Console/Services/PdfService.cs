﻿using Jma.Faxreplacement.ConsoleApp.Models;
using SelectPdf;
using System;
using System.Threading.Tasks;

namespace Jma.Faxreplacement.ConsoleApp.Services
{
    class PdfService
    {
        public static PdfSvcResponse CreatePdfFromHtml(string uid, string html, string folderPath, string filename)
        {
            try
            {
                SelectPdf.HtmlToPdf converter = new SelectPdf.HtmlToPdf();
                SelectPdf.PdfDocument doc = converter.ConvertHtmlString(html);
                doc.Save(folderPath + filename);
                doc.Close();
                return new PdfSvcResponse(uid, false, $"PDF ({filename}) created", null);
            }
            catch (Exception ex)
            {
                return new PdfSvcResponse(uid, true, ex.Message, ex);
            }
        }
        public static async Task<PdfSvcResponse> CreatePdfFromHtmlAsync(string uid, string html, string folderPath, string filename)
        {
            var t = Task<PdfSvcResponse>.Run(() => CreatePdfFromHtml(uid, html, folderPath, filename));
            var res = await t;
            return res;
        }

        public static PdfSvcResponse CombineFilesToSingle(string uid, string indir, string outdir, string[] filenames, string singlepdfname)
        {
            if (!singlepdfname.EndsWith(".pdf")) singlepdfname += ".pdf";
            for (var f = 0; f < filenames.Length; ++f)
            {
                filenames[f] = "\"" + filenames[f] + "\"";
            }
            string args1 = string.Join(" ", filenames);

            System.Diagnostics.Process process = new System.Diagnostics.Process
            {
                StartInfo = new System.Diagnostics.ProcessStartInfo()
                {
                    UseShellExecute = false,
                    CreateNoWindow = true,
                    WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden,
                    FileName = "pdftk.exe",
                    Arguments = args1 + " cat output " + outdir + singlepdfname,
                    RedirectStandardError = true,
                    RedirectStandardOutput = true
                }
            };
            try
            {
                process.Start();
                var res = process.StandardOutput.ReadToEnd();
                process.WaitForExit();
                if (process.ExitCode == 0)
                {
                    return new PdfSvcResponse(uid, false, $"Single pdf {singlepdfname} created.", null);
                }
                else
                {
                    var msg = $"Single pdf {singlepdfname} creation Unknown error.";
                    return new PdfSvcResponse(uid, true, msg, new Exception(msg));
                }
            }
            catch (Exception ex)
            {
                return new PdfSvcResponse(uid, true, $"Single pdf {singlepdfname} creation error. {ex.Message}", ex);
            }
            finally
            {
                process.Dispose();
            }
        }

        internal static int GetNumberOfPages(string outFolder, string fileName)
        {
            return PdfDocument.GetPagesCount(outFolder + fileName);
        }
    }

    public class PdfSvcResponse : SvcResponse
    {
        public PdfSvcResponse(string uid, bool isError, string errMsg, Exception e) : base(uid, isError, errMsg, e)
        {
        }
    }
}
